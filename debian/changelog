yadifa (3.0.2-3) unstable; urgency=medium

  * Expand libatomic patch

 -- Markus Schade <markus.schade@gmail.com>  Tue, 11 Mar 2025 19:11:29 +0100

yadifa (3.0.2-2) unstable; urgency=medium

  * Add libatomic-processor patch to fix armel build

 -- Markus Schade <markus.schade@gmail.com>  Tue, 11 Mar 2025 18:27:37 +0100

yadifa (3.0.2-1) unstable; urgency=medium

  * New upstream version 3.0.2

 -- Markus Schade <markus.schade@gmail.com>  Tue, 11 Mar 2025 15:35:22 +0100

yadifa (3.0.1-1) unstable; urgency=medium

  * New upstream version 3.0.1 (Closes: #1097324)

 -- Markus Schade <markus.schade@gmail.com>  Mon, 24 Feb 2025 08:11:41 +0100

yadifa (3.0.0-1) unstable; urgency=medium

  * New upstream version 3.0.0
  * Update d/copyright to 2024/2025
  * Remove no longer included install-sh from d/copyright

 -- Markus Schade <markus.schade@gmail.com>  Wed, 08 Jan 2025 10:12:07 +0100

yadifa (2.6.7-1) unstable; urgency=medium

  * New upstream version 2.6.7
  * Update d/copyright to 2024

 -- Markus Schade <markus.schade@gmail.com>  Thu, 05 Sep 2024 19:57:51 +0200

yadifa (2.6.5-1) unstable; urgency=medium

  * New upstream version 2.6.5
  * Update standards version to 4.6.2

 -- Markus Schade <markus.schade@gmail.com>  Tue, 21 Nov 2023 15:03:08 +0100

yadifa (2.6.4-1) unstable; urgency=medium

  * New upstream version 2.6.4 (Closes: #1032242)
  * Drop depends on lsb-base

 -- Markus Schade <markus.schade@gmail.com>  Thu, 02 Mar 2023 13:45:53 +0100

yadifa (2.6.3-1) unstable; urgency=medium

  * New upstream version 2.6.3

 -- Markus Schade <markus.schade@gmail.com>  Wed, 01 Mar 2023 13:18:35 +0100

yadifa (2.6.2-1) unstable; urgency=medium

  * New upstream version 2.6.2

 -- Markus Schade <markus.schade@gmail.com>  Thu, 19 Jan 2023 11:21:46 +0100

yadifa (2.6.0-2) unstable; urgency=medium

  * Include upstream patch to fix builds on big endian architectures

 -- Markus Schade <markus.schade@gmail.com>  Tue, 15 Nov 2022 14:11:19 +0100

yadifa (2.6.0-1) unstable; urgency=medium

  * New upstream version 2.6.0

 -- Markus Schade <markus.schade@gmail.com>  Mon, 07 Nov 2022 18:15:52 +0100

yadifa (2.5.4-1) unstable; urgency=medium

  * New upstream version 2.5.4
  * Update standards version to 4.6.1
  * Mark devel package Multi-Arch: foreign

 -- Markus Schade <markus.schade@gmail.com>  Fri, 20 May 2022 08:21:45 +0200

yadifa (2.4.2-1) unstable; urgency=medium

  * New upstream version 2.4.2 (Closes: #971605)
  * Paths and keyword changes to primary and secondary
  * Includes yakeyrolld - a utility to generate ZSK and KSK keys
  * Enable systemd-resolved-avoidance (not binding to 127.0.0.53 by default)
  * Add missing paths and capabilties to systemd service file

 -- Markus Schade <markus.schade@gmail.com>  Fri, 26 Feb 2021 17:37:43 +0100

yadifa (2.3.10-1) unstable; urgency=medium

  * New upstream version 2.3.10
  * Drop gcc 10 patch included upstream
  * Use secure URI in Homepage field.
  * Remove obsolete field Name from debian/upstream/metadata (already present
    in machine-readable debian/copyright)

 -- Markus Schade <markus.schade@gmail.com>  Sat, 26 Sep 2020 11:01:20 +0200

yadifa (2.3.9-1) unstable; urgency=medium

  * New upstream version 2.3.9
  * Add patch to built with gcc 10 (Closes: #958009)
  * Revert maintainer email as alioth is gone
  * Update to debhelper 13
  * Update standards version to 4.5.0
  * Update debian/watch
  * Add Upstream-Contact mailing list to d/copyright
  * Drop legacy .a files from -dev

 -- Markus Schade <markus.schade@gmail.com>  Mon, 27 Jul 2020 12:23:02 +0200

yadifa (2.3.8-1) unstable; urgency=medium

  * New upstream version 2.3.8
  * Update rules and compat to debhelper 11
  * Update standards version to 4.1.3
  * Update to watch version 4
  * Update Maintainer to Debian DNS Packaging team

 -- Markus Schade <markus.schade@gmail.com>  Sat, 24 Feb 2018 18:19:01 +0100

yadifa (2.3.7-1) unstable; urgency=medium

  * New upstream version 2.3.7
  * Drop no-compiledate patch in favor of new disable-build-timestamp option
  * Drop revert ppc64el patch now upstream

 -- Markus Schade <markus.schade@gmail.com>  Mon, 11 Dec 2017 15:59:52 +0100

yadifa (2.2.6-1) unstable; urgency=medium

  * New upstream version 2.2.6
    Closes: #876315, CVE-2017-14339
  * Refreshed no-compiledate patches
  * Update build dependency for debhelper

 -- Markus Schade <markus.schade@gmail.com>  Thu, 21 Sep 2017 11:59:20 +0200

yadifa (2.2.5-1) unstable; urgency=medium

  * New upstream version 2.2.5
  * Refreshed no-compiledate patches
  * Revert ppc64el compile patch. GCC issue resolved (Closes: #859016)
  * Remove -no-pie flag (Closes: #865700)

 -- Markus Schade <markus.schade@gmail.com>  Mon, 03 Jul 2017 10:58:43 +0200

yadifa (2.2.3-1) unstable; urgency=medium

  * New upstream version 2.2.3
  * Refreshed no-compiledate patches
  * Drop patches included upstream (except ppc64el referenced by upstream)

 -- Markus Schade <markus.schade@gmail.com>  Sat, 17 Dec 2016 23:20:54 +0100

yadifa (2.2.2-2) unstable; urgency=medium

  * Fix FTBFS on ppc64el due to a known gcc bug (#845751).
    Courtesy of Breno Leitao

 -- Markus Schade <markus.schade@gmail.com>  Mon, 28 Nov 2016 21:58:21 +0100

yadifa (2.2.2-1) unstable; urgency=medium

  * New upstream version 2.2.2 (Closes: #828612)
  * Refreshed no-compiledate patches
  * Update spelling fix patches

 -- Markus Schade <markus.schade@gmail.com>  Tue, 08 Nov 2016 12:21:48 +0100

yadifa (2.2.1-1) unstable; urgency=low

  * New upstream version 2.2.1
  * yadifad.conf
      rename keyword in acl to controller
      add example for dnssec policy
  * use /run instead of /var/run
  * Refreshed no-compiledate patches
  * Bump Standards-Version to 3.9.8

 -- Markus Schade <markus.schade@gmail.com>  Mon, 25 Jul 2016 10:11:58 +0200

yadifa (2.1.6-1) unstable; urgency=low

  * New upstream version 2.1.6
  * Changed Vcs-Git URI to https
  * Refreshed no-compiledate patches
  * Re-add patch fixing spelling errors
  * Add CPPFLAGS to properly produce dbgsym package
  * Re-order/fix configure options (tools) and enable dynamic provisioning
  * Declare compliance with Debian policy 3.9.7

 -- Markus Schade <markus.schade@gmail.com>  Fri, 19 Feb 2016 12:45:30 +0100

yadifa (2.1.4-2) unstable; urgency=low

  * Extend no-compile-date patch to make the build reproducible again.
    Courtesy of Reiner Herrmann. (Closes: #803139)

 -- Markus Schade <markus.schade@gmail.com>  Tue, 27 Oct 2015 11:39:00 +0100

yadifa (2.1.4-1) unstable; urgency=low

  * New upstream version 2.1.4

 -- Markus Schade <markus.schade@gmail.com>  Mon, 26 Oct 2015 15:00:10 +0100

yadifa (2.1.3-2) unstable; urgency=low

  * Add debian/patches/do-not-use-or-define-the-compile-date.patch
    to make the build reproducible. Courtesy of Santiago Vila
    (Closes: #798450)

 -- Markus Schade <markus.schade@gmail.com>  Wed, 09 Sep 2015 15:12:24 +0200

yadifa (2.1.3-1) unstable; urgency=low

  * New upstream version 2.1.3
  * Refresh patches for version 2.1.3
  * Add hardening flags to build

 -- Markus Schade <markus.schade@gmail.com>  Mon, 07 Sep 2015 19:31:05 +0200

yadifa (2.1.1-1) unstable; urgency=low

  * Initial release (Closes: #682716)

 -- Markus Schade <markus.schade@gmail.com>  Sun, 16 Aug 2015 15:54:44 +0200
